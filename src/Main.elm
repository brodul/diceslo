module Main exposing (main)

import Array
import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Encode exposing (string)
import Ports exposing (copyToClipboard)
import Random
import Words exposing (wordArray)



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MODEL


type alias Model =
    { randomWords : List String
    , password : String
    , initialSeed : Random.Seed
    , currentSeed : Random.Seed
    , numOfWords : Int
    }


init : Int -> ( Model, Cmd Msg )
init initialIntSeed =
    roll (Model [] "" (Random.initialSeed initialIntSeed) (Random.initialSeed initialIntSeed) 6)



-- UPDATE


type Msg
    = Roll
    | WordNumChange String
    | ClickToCopy


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Roll ->
            roll model

        WordNumChange str ->
            roll
                { model
                    | numOfWords =
                        case String.toInt str of
                            Nothing ->
                                6

                            Just int ->
                                int
                }

        ClickToCopy ->
            ( model, string model.password |> copyToClipboard )


diceGenerator : Random.Generator Int
diceGenerator =
    Random.int 0 7775


roll : Model -> ( Model, Cmd Msg )
roll model =
    let
        ( randomWords, newSeed ) =
            getWords model.numOfWords [] model.currentSeed
    in
    ( { model | randomWords = randomWords, currentSeed = newSeed, password = List.intersperse " " randomWords |> String.concat }
    , Cmd.none
    )



-- VIEW


getWords : Int -> List String -> Random.Seed -> ( List String, Random.Seed )
getWords wordNums recursiveList randomSeed =
    if wordNums <= 0 then
        ( recursiveList, randomSeed )

    else
        let
            ( randomWord, newSeed ) =
                getRandomWord randomSeed
        in
        getWords (wordNums - 1) (randomWord :: recursiveList) newSeed


getRandomWord : Random.Seed -> ( String, Random.Seed )
getRandomWord randomSeed =
    let
        ( int, newRandomSeed ) =
            Random.step diceGenerator randomSeed
    in
    ( getWordFromArray int, newRandomSeed )


getWordFromArray : Int -> String
getWordFromArray randomInt =
    case Array.get randomInt wordArray of
        Just string ->
            string

        Nothing ->
            ""


view : Model -> Html Msg
view model =
    div []
        [ div [ class "password-box" ]
            [ p [ onClick ClickToCopy ] [ text model.password ]
            ]
        , div []
            [ a [ onClick Roll, class "button" ] [ text "Ustvari geslo" ]

            -- , select [ onInput WordNumChange ]
            --     [ option [ value "4" ] [ text "4" ]
            --     , option [ value "5" ] [ text "5" ]
            --     , option [ value "6" ] [ text "6" ]
            --     ]
            ]
        ]
