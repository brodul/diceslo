// index.js

import { Elm } from './Main.elm'

const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};

const array = new Int32Array(1);
const app = Elm.Main.init({
    node: document.querySelector('main'),
    flags: window.crypto.getRandomValues(array)[0]
});

app.ports.copyToClipboard.subscribe(function (data) {
    const password = JSON.stringify(data).replace(/"/g, '');
    console.log(password);
    copyToClipboard(password);
});